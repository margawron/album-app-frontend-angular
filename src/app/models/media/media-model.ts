export class MediaDto {
    constructor(
        public id: number,
        public mimeType: string,
        public name: string,
        public ownerId: number,
        public tags: Array<number>
    ){}
}