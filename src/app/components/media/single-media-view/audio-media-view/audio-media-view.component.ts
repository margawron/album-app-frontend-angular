import { Component, Input, OnInit } from '@angular/core';
import { MediaService } from 'src/app/services/media/media.service';

@Component({
  selector: 'app-audio-media-view',
  templateUrl: './audio-media-view.component.html',
  styleUrls: ['./audio-media-view.component.scss']
})
export class AudioMediaViewComponent implements OnInit {

  @Input()
  mediaId: number|undefined;

  audioToShow: any;

  constructor(private mediaService: MediaService) { }

  ngOnInit() {
    if(this.mediaId){
      this.mediaService.getMediaBinaryData(this.mediaId).subscribe(blob => {
        let reader = new FileReader();
        reader.addEventListener("load", () => {
          this.audioToShow = reader.result;
        }, false)
        if(blob){
          reader.readAsDataURL(blob);
        }
      });
    }
  }

}
