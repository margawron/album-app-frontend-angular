import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AudioMediaViewComponent } from './audio-media-view.component';

describe('AudioMediaViewComponent', () => {
  let component: AudioMediaViewComponent;
  let fixture: ComponentFixture<AudioMediaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AudioMediaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AudioMediaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
