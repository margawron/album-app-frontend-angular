import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleMediaViewComponent } from './single-media-view.component';

describe('SingleMediaViewComponent', () => {
  let component: SingleMediaViewComponent;
  let fixture: ComponentFixture<SingleMediaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleMediaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleMediaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
