import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MediaDto } from 'src/app/models/media/media-model';
import { TagDto } from 'src/app/models/tag/tag-model';
import { MediaService } from 'src/app/services/media/media.service';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';
import { BinaryService } from 'src/app/services/binary/binary.service';
import { TagService } from 'src/app/services/tag/tag.service';

@Component({
  selector: 'app-single-media-view',
  templateUrl: './single-media-view.component.html',
  styleUrls: ['./single-media-view.component.scss']
})
export class SingleMediaViewComponent implements OnInit {

  @Input()
  mediaDto: MediaDto | undefined;

  @Input()
  creatingTagName = ""

  currentMediaTags = new BehaviorSubject<Array<TagDto>>([]);
  allUserTags = new BehaviorSubject<Array<TagDto>>([]);

  @Output()
  mediaChange = new EventEmitter();

  faRemoveBtn = faTimesCircle;

  constructor(
    private mediaService: MediaService,
    private binaryService: BinaryService,
    private tagService: TagService,
  ) { }
  
  ngOnInit(): void {
    const mediaId = this.mediaDto?.id;
    if (mediaId) {
      this.mediaService.getTagsForSingleMedia(mediaId).subscribe(tags => {
        this.currentMediaTags.next(tags)
      })
    }
    this.tagService.getAllUserTags().subscribe(tags => {
      this.allUserTags.next(tags)
    });
  }

  removeTag(tagId: number) {
    const mediaId = this.mediaDto?.id;
    if (mediaId) {
      this.mediaService.detachTagFromMedia(mediaId, tagId).subscribe()
      this.mediaChange.emit();
    }
  }

  removeMedia(mediaId: number | undefined) {
    if (mediaId) {
      this.mediaService.deleteMedia(mediaId).subscribe(() => {
        this.mediaChange.emit();
      })
      this.binaryService.removeBlob(mediaId);
    }
  }

  onTagInputKeyPress(event: KeyboardEvent) {
    const mediaId = this.mediaDto?.id
    if (event.key === "Enter" && mediaId) {
      const name = this.creatingTagName;
      this.tagService.getAllUserTags().subscribe(tags => {
        const optionalTag = tags.filter(x => x.name === name)[0];
        if(optionalTag){
          this.attachMediaToTag(mediaId, optionalTag.id, event);
        } else {
          this.tagService.createTag(name).subscribe(tag => {
            this.attachMediaToTag(mediaId, tag.id, event);
          })
        }
      })
    }
  }

  private attachMediaToTag(mediaId:number, tagId: number, event: KeyboardEvent){
    this.mediaService.attachTagToMedia(mediaId, tagId).subscribe(() => {
      this.mediaService.getTagsForSingleMedia(mediaId).subscribe(tags => {
        this.currentMediaTags.next(tags);
        (event.target as any).value = "";
        this.mediaChange.emit();
      })
    });
  }

  get isAudio(): boolean {
    const mimeType = this.mediaDto?.mimeType;
    if (mimeType) {
      const mimeTypeParts = mimeType.split("/") ?? [];
      return mimeTypeParts.length == 2 && mimeTypeParts[0] === "audio";
    }
    return false;
  }

  get isVideo(): boolean {
    const mimeType = this.mediaDto?.mimeType;
    if (mimeType) {
      const mimeTypeParts = mimeType.split("/") ?? [];
      return mimeTypeParts.length == 2 && mimeTypeParts[0] === "video";
    }
    return false;
  }

  get isImage(): boolean {
    const mimeType = this.mediaDto?.mimeType;
    if (mimeType) {
      const mimeTypeParts = mimeType.split("/") ?? [];
      return mimeTypeParts.length == 2 && mimeTypeParts[0] === "image";
    }
    return false;
  }

}
