import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { AuthInterceptor } from './interceptors/auth-interceptor';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ErrorModalComponent } from './components/error-modal/error-modal.component';
import { ErrorInterceptor } from './interceptors/error-interceptor';
import { LoginComponent } from './components/login/login.component';
import { NavComponent } from './components/nav/nav.component';
import { AllTagsViewComponent } from './components/tags/all-tags-view/all-tags-view.component';
import { AllMediaViewComponent } from './components/media/all-media-view/all-media-view.component';
import { UploadComponent } from './components/upload/upload.component';
import { RegisterComponent } from './components/register/register.component';
import { TagMediaViewComponent } from './components/media/tag-media-view/tag-media-view.component';
import { SingleMediaViewComponent } from './components/media/single-media-view/single-media-view.component';
import { ImageMediaViewComponent } from './components/media/single-media-view/image-media-view/image-media-view.component';
import { VideoMediaViewComponent } from './components/media/single-media-view/video-media-view/video-media-view.component';
import { AudioMediaViewComponent } from './components/media/single-media-view/audio-media-view/audio-media-view.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';

@NgModule({
  declarations: [
    AppComponent,
    ErrorModalComponent,
    LoginComponent,
    NavComponent,
    AllMediaViewComponent,
    TagMediaViewComponent,
    SingleMediaViewComponent,
    AllTagsViewComponent,
    UploadComponent,
    RegisterComponent,
    ImageMediaViewComponent,
    VideoMediaViewComponent,
    AudioMediaViewComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    RouterModule,
    FontAwesomeModule,
    NgbModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  },
  {
    provide: HTTP_INTERCEPTORS,
    useClass: ErrorInterceptor,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
