import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { MediaDto } from 'src/app/models/media/media-model';
import { MediaService } from 'src/app/services/media/media.service';
import { TagService } from 'src/app/services/tag/tag.service';

@Component({
  selector: 'app-tag-media-view',
  templateUrl: './tag-media-view.component.html',
  styleUrls: ['./tag-media-view.component.scss']
})
export class TagMediaViewComponent implements OnInit {

  mediasForTag = new BehaviorSubject<Array<MediaDto>>([]);

  constructor(
    private tagService: TagService,
    private route:ActivatedRoute
  ) { }

  ngOnInit() {
    this.loadMedia();
  }

  reloadMedia(){
    this.loadMedia();
  }

  loadMedia(){
    this.route.params.subscribe(params => {
      this.tagService.getAllMediaForTagWithId(params.id).subscribe(medias => {
        this.mediasForTag.next(medias);
      });
    });
  }

}
