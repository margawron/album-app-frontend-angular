export class TokenResponse{
    constructor(
        public token: string,
        public tokenType: string,
        public expirationDate: Date
    ) {}
}