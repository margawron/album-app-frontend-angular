import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AllMediaViewComponent } from './components/media/all-media-view/all-media-view.component';
import { AllTagsViewComponent } from './components/tags/all-tags-view/all-tags-view.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { UploadComponent } from './components/upload/upload.component';
import { TagMediaViewComponent } from './components/media/tag-media-view/tag-media-view.component';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';

const routes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "register", component: RegisterComponent },
  { path: "", children: [
    { path: "", component: AllMediaViewComponent },
    { path: "tags", component: AllTagsViewComponent },
    { path: "tags/:id", component: TagMediaViewComponent },
    { path: "media", component: AllMediaViewComponent },
    { path: "upload", component: UploadComponent }
  ], canActivate: [AuthGuardService]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    AuthGuardService,
  ]
})
export class AppRoutingModule { }
