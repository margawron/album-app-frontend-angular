import { HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, throwError } from "rxjs";
import { catchError } from "rxjs/operators";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ErrorModalComponent } from "../components/error-modal/error-modal.component";
import { ErrorModel } from "../models/error-model";
import { ErrorTranslationService } from "../services/error-translation/error-translation.service";


@Injectable({
    providedIn: 'root'
})
export class ErrorInterceptor implements HttpInterceptor {

    constructor(
        private modalService: NgbModal,
        private errorTranslationService: ErrorTranslationService
        ) { }
    isBlobError(err: any) {
        return err instanceof HttpErrorResponse && err.error instanceof Blob && err.error.type === 'application/json';
    }

    parseErrorBlob(err: HttpErrorResponse): Observable<any> {
        const reader: FileReader = new FileReader();
        const obs = new Observable((observer: any) => {
            reader.onloadend = (e) => {
                const newError = new HttpErrorResponse({
                    error: JSON.parse(reader.result as string),
                    headers: err.headers,
                    status: err.status,
                    statusText: err.statusText,
                    url: err.url|| "",
                });
                this.handleError(newError);
                observer.error(newError);
                observer.complete();
            };
        });
        reader.readAsText(err.error);
        return obs;
    }

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
        return next.handle(req).pipe(catchError(err => {
            if (!this.isBlobError(err)) {
              return this.handleError(err);
            } else {
              return this.parseErrorBlob(err);
            }
          }));
    }


    handleError(error: HttpErrorResponse) {
        switch (error.status) {
            case 0: {
                //only one modal even if more than 1 error to prevent modal spam
                const modalRef = this.modalService.open(ErrorModalComponent);
                modalRef.componentInstance.title = "Wystąpił błąd!";
                modalRef.componentInstance.message = "Brak połączenia z serwerem";
                break;
            }
            case 401: {
                //only one modal even if more than 1 error to prevent modal spam
                const modalRef = this.modalService.open(ErrorModalComponent);
                modalRef.componentInstance.title = "Wystąpił błąd!";
                modalRef.componentInstance.message = JSON.stringify(error);
                break;
            }
            case 403: {
                //only one modal even if more than 1 error to prevent modal spam
                const modalRef = this.modalService.open(ErrorModalComponent);
                modalRef.componentInstance.title = "Wystąpił błąd!";
                modalRef.componentInstance.message = "Brak dostępu.";
                break;
            }
            case 422: 
            case 400: {
                if (Array.isArray(error.error)) {
                    const errorArray = error.error as Array<ErrorModel>;
                    errorArray.forEach(el => {
                        const modalRef = this.modalService.open(ErrorModalComponent);
                        modalRef.componentInstance.title = "Wystąpił błąd!";
                        modalRef.componentInstance.message = this.errorTranslationService.translateKey(el.errorCode);
                    });
                } else {
                    const err = error.error as ErrorModel;
                    const modalRef = this.modalService.open(ErrorModalComponent);
                    modalRef.componentInstance.title = "Wystąpił błąd!";
                    modalRef.componentInstance.message = this.errorTranslationService.translateKey(err.errorCode);
                }
                break;
            }
            default: {
                //statements;
                break;
            }
        }
        return throwError(error);
    }

}