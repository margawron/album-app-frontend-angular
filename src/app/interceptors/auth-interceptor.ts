import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import { Observable } from "rxjs";

export class AuthInterceptor implements HttpInterceptor {

    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const token = localStorage.getItem("token");
        req.headers.set('Access-Control-Allow-Origin', '*');
        req.headers.set('Access-Control-Allow-Headers', 'Origin, Content-Type, X-Auth-Token, Location');
        req.headers.set('Access-Control-Allow-Methods', 'GET, POST, PATCH, PUT, DELETE, OPTIONS');
        req.headers.set('Access-Control-Expose-Headers', '*');

        if(token){
            const clonedHeader = req.clone({headers: req.headers.set("Authorization", "Bearer " + token)})
            return next.handle(clonedHeader);
        }
        return next.handle(req);
        }

}