import { HttpClient } from '@angular/common/http';
import { Injectable, OnInit } from '@angular/core';
import { MediaDto } from 'src/app/models/media/media-model';
import { TagDto } from 'src/app/models/tag/tag-model';
import { AbstractService } from '../abstract.service';

@Injectable({
  providedIn: 'root'
})
export class MediaService extends AbstractService implements OnInit {

  constructor(httpClient: HttpClient) {
    super(httpClient, "media")
  }

  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

  getAllLoggedUserMedia(){
    return this.httpClient.get<MediaDto[]>(this.serviceUrl + "/self");
  }

  getSingleMedia(mediaId: number){
    return this.httpClient.get<MediaDto>(this.serviceUrl + "/" + mediaId)
  }

  getTagsForSingleMedia(mediaId: number){
    return this.httpClient.get<TagDto[]>(this.serviceUrl + `/${mediaId}/tags`);
  }

  getMediaBinaryData(mediaId: number){
    return this.httpClient.get(this.serviceUrl + "/binary/" + mediaId, {responseType: 'blob'});
  }

  deleteMedia(mediaId: number){
    return this.httpClient.delete(this.serviceUrl + "/" + mediaId);
  }

  uploadImage(file: File){
    const formData = new FormData();
    formData.append("file", file, file.name);
    return this.httpClient.request<MediaDto>("POST", this.serviceUrl + "/upload", {body: formData, observe: "response"});
  }

  attachTagToMedia(mediaId: number, tagId: number){
    return this.httpClient.post<MediaDto>(this.serviceUrl + `/${mediaId}/tag/attach/${tagId}`, {});
  }

  detachTagFromMedia(mediaId: number, tagId: number){
    return this.httpClient.post<MediaDto>(this.serviceUrl + `/${mediaId}/tag/detach/${tagId}`, {});
  }
}
