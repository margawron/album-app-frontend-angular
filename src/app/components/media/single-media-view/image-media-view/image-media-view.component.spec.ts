import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageMediaViewComponent } from './image-media-view.component';

describe('ImageMediaViewComponent', () => {
  let component: ImageMediaViewComponent;
  let fixture: ComponentFixture<ImageMediaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageMediaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageMediaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
