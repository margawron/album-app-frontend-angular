import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MediaDto } from 'src/app/models/media/media-model';
import { AuthService } from 'src/app/services/login/login.service';
import { MediaService } from 'src/app/services/media/media.service';

@Component({
  selector: 'app-all-media-view',
  templateUrl: './all-media-view.component.html',
  styleUrls: ['./all-media-view.component.scss']
})
export class AllMediaViewComponent implements OnInit {

  allUserMedia = new BehaviorSubject<Array<MediaDto>>([]);
  images: Array<Blob> = [];

  constructor(
    private mediaService: MediaService,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.loadMedias();
  }

  reloadDtos() {
    this.loadMedias();
  }

  loadMedias() {
    if (this.authService.isLoggedIn()) {
      this.mediaService.getAllLoggedUserMedia().subscribe(dtos => {
        this.allUserMedia.next(dtos);
      })
    }
  }

}
