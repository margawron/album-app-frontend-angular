import { Component, ElementRef, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { MediaDto } from 'src/app/models/media/media-model';
import { MediaService } from 'src/app/services/media/media.service';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})
export class UploadComponent {


  @ViewChild("inputDialog")
  imageInput: ElementRef | null;

  uploadedFile = new BehaviorSubject<MediaDto|null>(null);

  constructor(private mediaService:MediaService) { 
    this.imageInput = null;    
  }

  addMedia(){
    const elem: HTMLElement = this.imageInput?.nativeElement;
    elem.click();
  }

  uploadFile(){
    const fileToUpload: any = this.imageInput?.nativeElement;
    this.mediaService.uploadImage(fileToUpload.files[0]).subscribe(response => {
      this.uploadedFile.next(response.body);
    });
  }

}
