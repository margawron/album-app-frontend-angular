import { ThrowStmt } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { MediaService } from '../media/media.service';

@Injectable({
    providedIn: 'root',
})
export class BinaryService {
    private blobs: Map<number, Blob>;

    constructor(private mediaService: MediaService) {
        this.blobs = new Map<number, Blob>();
    }

    removeBlob(mediaId:number){
        this.blobs.delete(mediaId);
    }

    getBlob(mediaId: number): Observable<Blob>{
        if (this.blobs.has(mediaId)) {
            return new Observable(observable => {
                observable.next(this.blobs.get(mediaId))
                observable.complete();
            });
        } else {
            return this.mediaService
                .getMediaBinaryData(mediaId)
                .pipe(map(blob => {
                    if(blob.size < 5_000_000){
                        this.blobs.set(mediaId, blob);
                    }
                    return blob;
                }))
        }
    }
}
