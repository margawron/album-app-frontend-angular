export class TagDto {
    constructor(
        public id: number,
        public name: string,
        public ownerId: number,
        public medias: Array<number>,
    ){}
}