import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ErrorTranslationService {

  private errorCodeToErrorMessages: IDictionary<string> = {}; 

  constructor() { 
    this.errorCodeToErrorMessages["user.exists"] = "Użytkownik o podanych danych już istnieje"
    this.errorCodeToErrorMessages["user.notExists"] = "Użytkownik o podanych danych nie istnieje"
    this.errorCodeToErrorMessages["user.password.invalid"] = "Hasło jest nieprawidłowe"
  }

  translateKey(key: string): string {
    return this.errorCodeToErrorMessages[key];
  }
}

interface IDictionary<Val>{
  [key: string]: Val
}
