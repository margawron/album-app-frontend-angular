export class UserRegisterRequest {
    constructor(
        public login: string,
        public password: string,
        public email: string
    ){}
}