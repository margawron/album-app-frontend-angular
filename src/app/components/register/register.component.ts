import { Component } from '@angular/core';
import { AbstractControlOptions, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { MustMatch } from '../../helpers/must-match';
import { UserRegisterRequest } from 'src/app/models/user/user-register-request';
import { AuthService } from 'src/app/services/login/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent {

  registerForm: FormGroup;
  submitted: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private modalService: NgbModal,
    private router: Router
    ) {
    this.registerForm = this.formBuilder.group({
      login: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(12)]],
      mail: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30), Validators.email]],
      password: ['', [Validators.required, Validators.minLength(5), Validators.maxLength(30)]],
      confirmPass: ['', [Validators.required, MustMatch('password', 'confirmPass')]],
    });
    this.submitted = false;
  }

  get f() {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = true;
    if (this.registerForm.invalid) {
      return;
    }

    console.log(this.registerForm)
    const userRegisterRequest: UserRegisterRequest = new UserRegisterRequest(
      this.registerForm.get("login")?.value,
      this.registerForm.get("password")?.value,
      this.registerForm.get("mail")?.value,
    );

    this.authService.register(userRegisterRequest).subscribe(registerResponse => {
      this.router.navigate(["login"])
    });
  }

}
