export class RegisterResponse{
    constructor(
        public id: number,
        public username: string,
        public email: string,
        public role: string,
        public creationDate: Date,
        public userAccountState: number,
        public deactivationDate: Date, 
        public media: Array<number>,
        public tags: Array<number>
    ){}
}