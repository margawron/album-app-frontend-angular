import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TagMediaViewComponent } from './tag-media-view.component';

describe('TagMediaViewComponent', () => {
  let component: TagMediaViewComponent;
  let fixture: ComponentFixture<TagMediaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TagMediaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TagMediaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
