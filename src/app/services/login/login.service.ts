import { HttpClient, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoggedUser } from 'src/app/models/user/logged-user';
import { TokenResponse } from 'src/app/models/token/token-response';
import { TokenRequest } from 'src/app/models/token/token-request';
import { AbstractService } from '../abstract.service';
import { UserRegisterRequest } from 'src/app/models/user/user-register-request';
import { RegisterResponse } from 'src/app/models/user/register-response';

@Injectable({
    providedIn: 'root'
})
export class AuthService extends AbstractService {

    private hostAddress = environment.hostAddress;
    private jwtHelper = new JwtHelperService();
    private loggedUser: LoggedUser | any;

    constructor(http: HttpClient, private router: Router) {
        super(http, "auth");
        try {
            this.loggedUser = JSON.parse(localStorage.getItem('loggedUser')!);
            const token = localStorage.getItem('token')!;
            if (this.jwtHelper.isTokenExpired(token)) {
                localStorage.removeItem('token');
                localStorage.removeItem('loggedUser');
            }
        } catch {
            this.loggedUser = null;
        }
    }

    login(username: string, password: string) {
        const loginModel: TokenRequest = { username: username, password: password };
        const serviceUrl = this.serviceUrl + '/login';        
        this.httpClient.post<TokenResponse>(serviceUrl, loginModel).subscribe(res => {
            const claims = this.jwtHelper.decodeToken(res.token);
            this.loggedUser = new LoggedUser(claims["username"], claims["role"]);
            
            localStorage.setItem('token', res.token);
            localStorage.setItem('loggedUser', JSON.stringify(this.loggedUser));
            this.router.navigate(['/']);
        });
    }

    register(userRegisterRequest: UserRegisterRequest){
        return this.httpClient.post<RegisterResponse>(this.serviceUrl + "/register", userRegisterRequest).pipe(registerReponse => {
            console.log(registerReponse)
            return registerReponse;
        });
    }

    public logout() {
        localStorage.removeItem('token')
        localStorage.removeItem('loggedUser');
    }

    public isLoggedIn() {
        const token = localStorage.getItem('token')

        if (token == null) {
            return false;
        } else if (this.jwtHelper.isTokenExpired(token)) {
            localStorage.removeItem('token');
            localStorage.removeItem('loggedUser');
            this.loggedUser = null;
            return false;
        }
        return true;
    }

    getLoggedUser(): LoggedUser {
        return this.loggedUser;
    }

    getLoggedUsername(): string {
        return this.loggedUser.username;
    }

    get isModerator(): boolean {
        const role = this.getLoggedUser()?.role;
        return role === "Admin" || role === "User";
    }

    get isAdmin(): boolean {
        const role = this.getLoggedUser()?.role;
        return role === "Admin";
    }
}
