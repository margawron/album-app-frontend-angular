import { AbstractControl, ValidationErrors, ValidatorFn } from '@angular/forms';

// custom validator to check that two fields match
export function MustMatch(controlName: string, matchingControlName: string): ValidatorFn {
    return (control: AbstractControl): ValidationErrors | null => {
        const firstControl = control.get(controlName);
        const secondControl = control.get(matchingControlName);

        if(firstControl?.errors && !secondControl?.errors){
            return null;
        }

        if(firstControl?.value !== secondControl?.value){
            return {
                mustMatch: true
            };
        }
        return null;
    };
}