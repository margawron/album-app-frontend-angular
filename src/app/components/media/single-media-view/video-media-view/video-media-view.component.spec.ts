import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoMediaViewComponent } from './video-media-view.component';

describe('VideoMediaViewComponent', () => {
  let component: VideoMediaViewComponent;
  let fixture: ComponentFixture<VideoMediaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VideoMediaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoMediaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
