import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { TagDto } from 'src/app/models/tag/tag-model';
import { TagService } from 'src/app/services/tag/tag.service';

@Component({
  selector: 'app-all-tags-view',
  templateUrl: './all-tags-view.component.html',
  styleUrls: ['./all-tags-view.component.scss']
})
export class AllTagsViewComponent implements OnInit {

  allTags = new BehaviorSubject<Array<TagDto>>([])
  newTagName: string = "";

  @ViewChild("tagName")
  tagNameElementRef: ElementRef|null = null;


  constructor(private tagService: TagService) { }

  ngOnInit(): void {
    this.tagService.getAllUserTags().subscribe(tags => {
      this.allTags.next(tags);
    })
  }

  newTagNameChange(event: any){
    this.newTagName = event.target?.value;
  }


  createTag(){
    if(this.newTagName !== ""){
      this.tagService.createTag(this.newTagName).subscribe(tag => {
        const that = this.tagNameElementRef;
        (that as any).nativeElement.value = "";
        this.tagService.getAllUserTags().subscribe(tags => {
          this.allTags.next(tags);
        });
        
      }) 
    }
  }

  deleteTag(id: number){
    this.tagService.deleteTag(id).subscribe(() => {
      this.tagService.getAllUserTags().subscribe(tags => {
        this.allTags.next(tags);
      });
    })
  }

}
