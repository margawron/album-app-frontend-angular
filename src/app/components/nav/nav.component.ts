import { Component, OnInit, Type } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { AuthService } from 'src/app/services/login/login.service';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {


  currentUrl: string = ""

  constructor(
    private authService: AuthService, 
    private router: Router,
    ){}

  ngOnInit(): void {
     this.router.events.subscribe(event => {
       if(event instanceof NavigationEnd){
         this.currentUrl = event.url;
       }
     })
  }

  isLoggedIn(){
    return this.authService.isLoggedIn();
  }

  get loggedUsername(){
    return this.authService.getLoggedUsername();
  }

  logout() {
    this.authService.logout();
    window.location.reload();
  }

  isUploadComponent(): boolean {
    return this.currentUrl.includes("upload");
  }

  isTagsComponent():boolean {
    return this.currentUrl.includes("tags");
  }

  isMediaComponent():boolean{
    return this.currentUrl.includes("media");
  }

  isAuthComponent():boolean {
    return this.currentUrl.includes("login") || this.currentUrl.includes("register");
  }

}
