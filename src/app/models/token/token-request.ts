export class TokenRequest {
    constructor(
        public username: string,
        public password: string
    ) {}
}