import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AllMediaViewComponent } from './all-media-view.component';

describe('AllMediaViewComponent', () => {
  let component: AllMediaViewComponent;
  let fixture: ComponentFixture<AllMediaViewComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AllMediaViewComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AllMediaViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
