import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/login/login.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  constructor(private authService: AuthService){}

  get isAuthenticated(){
    return this.authService.isLoggedIn();
  }
  title = 'Album';
}
