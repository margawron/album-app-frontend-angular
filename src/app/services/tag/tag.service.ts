import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { MediaDto } from 'src/app/models/media/media-model';
import { TagDto } from 'src/app/models/tag/tag-model';
import { AbstractService } from '../abstract.service';

@Injectable({
  providedIn: 'root'
})
export class TagService extends AbstractService {

  constructor(httpClient: HttpClient) {
    super(httpClient, "tag")
  }

  createTag(tagName: string) {
    return this.httpClient.post<TagDto>(this.serviceUrl, { "TagName": tagName });
  }

  deleteTag(tagId: number) {
    return this.httpClient.delete<null>(this.serviceUrl + "/" + tagId);
  }

  getTagById(tagId: number) {
    return this.httpClient.get<TagDto>(this.serviceUrl + "/" + tagId);
  }

  getAllUserTags() {
    return this.httpClient.get<TagDto[]>(this.serviceUrl + "/self");
  }

  getAllMediaForTagWithId(tagId: number) {
    return this.httpClient.get<MediaDto[]>(this.serviceUrl + `/${tagId}/media`)
  }
}
