import { Component, Input, OnInit } from '@angular/core';
import { MediaService } from 'src/app/services/media/media.service';

@Component({
  selector: 'app-video-media-view',
  templateUrl: './video-media-view.component.html',
  styleUrls: ['./video-media-view.component.scss']
})
export class VideoMediaViewComponent implements OnInit {

  @Input()
  mediaId: number|undefined;

  videoToShow: any;

  constructor(private mediaService: MediaService) { }

  ngOnInit() {
    if(this.mediaId){
      this.mediaService.getMediaBinaryData(this.mediaId).subscribe(blob => {
        let reader = new FileReader();
        reader.addEventListener("load", () => {
          this.videoToShow = reader.result;
        }, false)
        if(blob){
          reader.readAsDataURL(blob);
        }
      });
    }
  }

}
