import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { BinaryService } from 'src/app/services/binary/binary.service';
import { MediaService } from 'src/app/services/media/media.service';
import { isBigIntLiteral } from 'typescript';

@Component({
  selector: 'app-image-media-view',
  templateUrl: './image-media-view.component.html',
  styleUrls: ['./image-media-view.component.scss']
})
export class ImageMediaViewComponent implements OnInit {

  @Input()
  mediaId: number|undefined;

  imageToShow: any;

  constructor(private binaryService: BinaryService) { }

  ngOnInit() {
    if(this.mediaId){
      this.binaryService.getBlob(this.mediaId).subscribe(blob => {
        let reader = new FileReader();
        reader.addEventListener("load", () => {
          this.imageToShow = reader.result;
        }, false)
        if(blob){
          reader.readAsDataURL(blob);
        }
      });
    }
  }

}
