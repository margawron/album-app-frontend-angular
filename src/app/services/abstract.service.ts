import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";


export class AbstractService{
    BASE_URL: string;
    constructor(protected httpClient: HttpClient, private domainUrl: string){
        this.BASE_URL = environment.hostAddress;
    }

    get serviceUrl(){
        return this.BASE_URL + this.domainUrl;
    }

}